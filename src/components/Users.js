import React, { Component } from 'react'
import Userdetails from './Userdetails'
class Users extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      users:[],
    }
  }
  componentDidMount(){
    fetch('https://jsonplaceholder.typicode.com/users')
    .then(res=>res.json())
    .then(data=>{
       const users=data.slice(0,10)
       console.log(users);
        this.setState({
            users,
        })
    })
  }
  
  render() {
    let {users}=this.state
    return (
      <div className='user-box'>
        {users.map(ele=>{
            return <Userdetails user={ele} key={ele.id}/>
        })}
      </div>
    )
  }
}

export default Users