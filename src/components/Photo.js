import React from 'react'

function Photo(props) {
  let {photo}=props
  return (
    <div className='photo-cont'>
        <img src={photo.url} alt='userphoto'></img>
       <div className='smheading'>{photo.title}</div>
    </div>
  )
}

export default Photo