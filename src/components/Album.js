import React, { Component } from 'react'
import Photo from './Photo'
class Album extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         albumTitle:"",
         photos:[]
      }
    }
    componentDidMount(){
    let {id}=this.props
    fetch(`https://jsonplaceholder.typicode.com/photos?albumId=${id}`)
    .then(res=>res.json())
    .then(data=>{
       const photos=data.slice(0,5)
        this.setState({
            ...this.state,
            photos,
        })
    })
    fetch(`https://jsonplaceholder.typicode.com/albums?userId=${id}`)
    .then(res=>res.json())
    .then(data=>{
       const album=data[0]
        this.setState({
            ...this.state,
            albumTitle:album.title
        })
    })
    }
  render() {
    let{albumTitle,photos}=this.state
    return (
      <div >
        <div className='album-title'>{albumTitle}</div>
        <div className='album-cont'>
        {photos.map(photo=>{
            return <Photo photo={photo} key={photo.id}/>
        })}
        </div>
      </div>
    )
  }
}

export default Album