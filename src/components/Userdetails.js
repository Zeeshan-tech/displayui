import React from "react";
import Album from "./Album";
function Userdetails(props) {
  let { user } = props;
  return (
    <div >
      <div className="user-card">
        <div>
          <span className="user-title">{user.name}</span>
        </div>
        <div>
          <span className="user-title">{user.email}</span>
        </div>

        <div >
          <span className="user-title">Address</span>
        </div>
        <div >
          <span className="user-title"> street :</span> {user.address.street}
        </div>
        <div>
          <span className="user-title"> city :</span> {user.address.city}
        </div>
        <div>
          <span className="user-title">zip: </span>
          {user.address.zipcode}
        </div>

        <div>
          <span className="user-title">phone</span> : {user.phone}
        </div>
        <div>
          <span className="user-title">website </span>
          <a href="hildegard.org">hildegard.org</a>
        </div>
        <div>
          <span className="user-title">company :</span> {user.company.name}
        </div>
      </div>
      <Album id={user.id} />
    </div>
  );
}

export default Userdetails;
